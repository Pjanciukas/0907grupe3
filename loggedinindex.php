<?php
	include ('session.php');
	$dberror = null;
	$currentuser = $login_session;
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		switch($_POST['action']) {
			case 'postnews':
			$comment_status = "allowed";
			if($login_type == 'admin'){
				$comment_status = $_POST['comment-status'];
			}
			$text = $_POST['content'];
			$text = stripslashes($text);
			$text = mysqli_real_escape_string($db, $text);
			$article_name = $_POST['article_name'];
			$article_name = stripslashes($article_name);
			$article_name = mysqli_real_escape_string($db, $article_name);
			$file = rand(1000,100000)."-".$_FILES['file']['name'];
			$file_loc = $_FILES['file']['tmp_name'];
			$folder="images/";
			$final_file=str_replace(' ','-',strtolower($file));
			$sql = "INSERT INTO user_content (username, article_name, text, image, comment_status) VALUES ('" . $currentuser . "', '" . $article_name . "', '" . $text . "', '" . $final_file . "', '" . $comment_status . "')";
			if(move_uploaded_file($file_loc,$folder.$final_file) & ($db->query($sql) === FALSE))
			{ 
				$dberror = "Database error";
			}
			
			break;
			case 'deletenews':
				$post_id=$_POST['var'];
				$sql = "DELETE FROM user_content WHERE id='{$post_id}'";
				$db->query($sql);
			break;
		}
		
	}
	
	$sql = "SELECT text, article_name, id, date, image FROM user_content WHERE username='{$currentuser}' ORDER BY DATE DESC";
	$userposts = $db->query($sql); 
?>


<!DOCTYPE HTML>
<html>
    <head>
        <title>Sveiki, <?php echo $login_session; ?></title>
        <meta charset="utf-8" />
		<link rel="shortcut icon" href="images/favicon.ico"/>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:'textarea' });</script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
			<?php if($login_type == 'admin'){
				include ('adminheader.php');
			} else {
				include('userheader.php');
			}
			?>
            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
					<p>Čia galite rašyti naujienas</p>
					<form method="post" action="" enctype="multipart/form-data">
						<div class="row uniform 50%">
                                <div class="6u 12u$(xsmall)">
									<input type="hidden" name="action" value="postnews">
                                    <input type="text" name="article_name" id="article_name" value="" placeholder="Straipsnio pavadinimas" required/>
                                </div>
						</div>
						<br>
						<textarea name="content">Straipsnio turinys</textarea>
						<br>
						<input type="file" name="file" required/>
						<br>
						<?php if($login_type == 'admin'){ ?>
							<select name="comment-status" id="comment-status-style">
							  <option value="allowed">Leisti komentuoti</option>
							  <option value="disallowed">Neleisti komentuoti</option>
							  <option value="approved-only">Rodyti tik administratoriaus patvirtintus komentarus</option>
							</select>
						<?php } ?>
						<input type="submit" value="Prideti!" class="special" />
					</form>
  
                    <h3>Jūsų naujienos:</h3>
					<?php while($post = $userposts->fetch_assoc()){
						$post_id = $post['id'];
					?>
					<div id="news" align="center" class="div_news">
						<a href="http://localhost/0907grupe3/news_page.php?postid=<?php echo $post_id?>"><h4><?php echo $post['article_name'] ?></h4></a>
						<p><?php echo strip_tags($post['text']); ?> </p>
						<img src="images/<?php echo strip_tags($post['image']);?> " height="150" width="150"><br>
						<span>Atnaujinta: <?php echo strip_tags($post['date']); ?></span><br>
						<a href="http://localhost/0907grupe3/update_news.php?postid=<?php echo $post_id?>"><button type="button" class="btn btn-default">Update Article</button></a>
						<form method="post" action='' class="deletebtn">
							<input type="submit" value="Delete Article" class="special" />
							<input type="hidden" name="action" value="deletenews">
							<input type="hidden" name="var" value='<?php echo "$post_id";?>'>
						</form>
					</div>
					<br>
					<?php
					}
                    ?>
                </div>
            </section>

            

            <!-- Footer -->
            <?php include ('footer.php'); ?>

        </div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>