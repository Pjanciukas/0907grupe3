<header id="header">
                <h5 id="logo"><a href="index.php">Pagrindinis puslapis</a></h5>
                <nav id="nav">
                    <ul>
                        <li>
                            <a href="#">Admin'o veiksmai</a>
                            <ul>
                                <li><a href="admin_modify_news.php">Naujienų valdymas</a></li>
                                <li><a href="admin_ban_users.php">Banninti vartotojus</a></li>
								<li><a href="admin_statistic.php">Tinklalapio statistika</a></li>
                            </ul>
                        </li>
						<li><a href="loggedinindex.php" class="button special">Vartotojo Profilis</a></li>
						<li><a href="logout.php" class="button special">Atsijungti</a></li>
                    </ul>
                </nav>
</header>