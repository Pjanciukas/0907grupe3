<?php
	include ('session.php');
	$dberror = null;
	$currentuser = $login_session;
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		$_SESSION['selected_user'] = $_GET['user'];
	}

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$post_id=$_POST['var'];
			$sql = "DELETE FROM user_content WHERE id='{$post_id}'";
			$db->query($sql);

		}
	$sql = "SELECT * FROM user_content WHERE username='{$_SESSION['selected_user']}' ORDER BY date DESC";
	$results = $db->query($sql); 
?>


<!DOCTYPE HTML>
<html>
    <head>
        <title>Welcome <?php echo $login_session; ?></title>
        <meta charset="utf-8" />
		<link rel="shortcut icon" href="images/favicon.ico"/>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:'textarea' });</script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body class="landing"> 
	
        <div id="page-wrapper">

            <!-- Header -->
			<?php include ('adminheader.php'); ?>
			
            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
					<h3><?php echo $_SESSION['selected_user'] ?> posts:</h3>
					<?php while($post = $results->fetch_assoc()){
						$post_id = $post['id'];
					?>
					<div id="news" align="center" class="div_news">
						<a href="http://localhost/0907grupe3/news_page.php?postid=<?php echo $post_id?>"><h4><?php echo $post['article_name'] ?></h4></a>
						<p><?php echo strip_tags($post['text']); ?> </p>
						<img src="images/<?php echo strip_tags($post['image']);?> " height="150" width="150"><br>
						<span>Atnaujinta: <?php echo strip_tags($post['date']); ?></span><br>
						<a href="http://localhost/0907grupe3/update_news.php?postid=<?php echo $post_id?>"><button type="button" class="btn btn-default">Update Article</button></a>
						<form method="post" action='' class="deletebtn">
							<input type="submit" value="Delete Article" class="special" />
							<input type="hidden" name="action" value="deletenews">
							<input type="hidden" name="var" value='<?php echo "$post_id";?>'>
						</form>
					</div>
					<?php
						}
					?>
					
					<br>

                </div>
            </section>

            

            <!-- Footer -->
            <?php include ('footer.php'); ?>

        </div>

        <!-- Scripts -->
		
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>