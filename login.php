<?php
	session_start();
	include("config.php");
	$error = null;
	$ban_error = null;
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$username = $_POST['username'];
		$password = $_POST['password'];
		//security reasons
		$username = stripslashes($username);
		$password = stripslashes($password);
		$username = mysqli_real_escape_string($db, $username);
		$password = mysqli_real_escape_string($db, $password);
		
		$sql = "SELECT banned FROM users WHERE username='{$username}' and password='{$password}'";
		$results = $db->query($sql); 
		if ($results->num_rows > 0) { 
			$result = $results->fetch_assoc();
			if($result['banned'] == "No"){
			$_SESSION['username'] = $username;
			header("Location: loggedinindex.php");
			} else {
				$ban_error = "User is banned";
			}
		} else { 
			$error = "Your Login Name or Password is invalid";
		}
	}
?>
<html>
    <head>
        <title>Prisijungimas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="images/favicon.ico"/>
        <link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
                <h5 id="logo"><a href="index.php">Pagrindinis puslapis</a></h5>
                <nav id="nav">
                    <ul>
                        <li><a href="register.php" class="button special">Registruotis</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
                        <h2>Prisijungimas</h2>
                        <?php
                        if ($error != null) {
                            ?>
                            <p class="actions">Blogas slaptazodis</p>	
                            <?php
                        }
                        ?>
						<?php
                        if ($ban_error != null) {
                            ?>
                            <p class="actions">Vartotojas uzblokuotas</p>	
                            <?php
                        }
                        ?>
                        <form method="post" action="login.php">
                            <div class="row uniform ">
                                <div class="6u 12u$(xsmall)">
                                    <input type="text" name="username" id="username" value="" placeholder="Prisijungimo vardas" />
                                </div>
                                <div class="6u$ 12u$(xsmall)">
                                    <input type="password" name="password" id="password" value="" placeholder="Slaptazodis" />
                                </div>
                                <div class="12u$">
                                    <ul class="actions">
                                        <li><input type="submit" value="Prisijungti" class="special" /></li>
                                    </ul>
                                </div>
                            </div>
                        </form>

                </div>
            </section>

<!-- Footer -->
			<?php include ("footer.php"); ?>
<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/jquery.scrolly.min.js"></script>
            <script src="assets/js/jquery.dropotron.min.js"></script>
            <script src="assets/js/jquery.scrollex.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>

    </body>
</html>
