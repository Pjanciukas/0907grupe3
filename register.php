<?php
	session_start(); 
	include("config.php");
	$error = null;
	$passworderror = null;
	$dberror = null;
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$username = $_POST['username'];
		$password = $_POST['password'];
		$retypedpassword = $_POST['retypedpassword'];
		if($password == $retypedpassword){
			$username = stripslashes($username);
			$password = stripslashes($password);
			$username = mysqli_real_escape_string($db, $username);
			$password = mysqli_real_escape_string($db, $password);
			$sql = "SELECT id FROM users WHERE username='{$username}'";
			$result = $db->query($sql); 
			if ($result->num_rows > 0) {
				$error = "Vartotojas egzistuoja";
			} else {
				$sql = "INSERT INTO users (username, password) VALUES ('" . $username . "', '" . $password . "')";
				if($db->query($sql) === TRUE) {
					$_SESSION['username']=$username;
					header("location: loggedinindex.php");
				} else {
					$dberror = "Database error";
				}
			}
		} else { 
			$passworderror = "Your Login Name or Password is invalid";
		}
	}
?>

<html>
    <head>
        <title>Registracija</title>
        <meta charset="utf-8" />
		<link rel="shortcut icon" href="images/favicon.ico"/>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
                <h5 id="logo"><a href="index.php">Pagrindinis Puslapis</a></h5>
                <nav id="nav">
                    <ul>
                        <li><a href="login.php" class="button special">Prisijungti</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
                        <h2>Registracija</h2>
                        <?php
                        if ($error != null) {
                        ?>
                            <p class="actions">Vartotojas jau egzistuoja</p>	
                        <?php
                        } else if($passworderror != null){
						?>
							<p class="actions">Slaptazodziai nesutampa</p>
						<?php
						} else if($dberror != null){
						?>
							<p class="actions">Ivyko klaida bandant issaugoti jusu duomenis!</p>
						<?php
						}
						?>
                        <form method="post" action="register.php">
                            <div class="row uniform 50%">
                                <div class="6u 12u$(xsmall)">
                                    <input type="text" name="username" id="username" value="" placeholder="Prisijungimo vardas" />
                                </div>
								
                                <div class="6u$ 12u$(xsmall)">
                                    <input type="password" name="password" id="password" value="" placeholder="Slaptazodis" />
                                </div>
								<div class="6u$ 12u$(xsmall)">
                                    <input type="password" name="retypedpassword" id="retypedpassword" value="" placeholder="Pakartoti slaptazodi" />
                                </div>
                                <div class="12u$">
                                    <ul class="actions">
                                        <li><input type="submit" value="Registruotis" class="special" /></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                </div>
            </section>
          
<!-- Footer -->
	<?php include ("footer.php"); ?>
<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/jquery.scrolly.min.js"></script>
            <script src="assets/js/jquery.dropotron.min.js"></script>
            <script src="assets/js/jquery.scrollex.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>

    </body>
</html>