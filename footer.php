			<footer id="footer">
                <ul class="icons">
                    <li><a href="https://twitter.com/4Pasaulis" class="icon alt fa-twitter" target="_blank"><span class="label">Twitter</span></a></li>
                    <li><a href="https://www.facebook.com/Pasaulio-naujienos-1759290497667479/posts/" class="icon alt fa-facebook" target="_blank"><span class="label">Facebook</span></a></li>
                    
                    <li><a href="mailto:4naujienos@gmail.com" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; Nekopijuok. Visos teisės saugomos.</li><li>Sukūrė: Šarūnas, Kęstas, Mantas</a></li>
                </ul>
            </footer>