<?php
	include ('session.php');

	$dberror = null;
	$update_success = null;
	$currentuser = $login_session;
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		$_SESSION['comment_id'] = $_GET['comment_id'];
	}
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$text = $_POST['content'];
		$text = stripslashes($text);
		$text = mysqli_real_escape_string($db, $text);
		$name = $_POST['name'];
		$name = stripslashes($name);
		$name = mysqli_real_escape_string($db, $name);
		$sql = "UPDATE comments SET name = '{$name}', text = '{$text}' WHERE id={$_SESSION['comment_id']}";
		if($db->query($sql) === FALSE) {
		$dberror = "Database error";
		} else {
			$update_success = "Update complete";
		}
	}
	
	$sql = "SELECT name, text FROM comments WHERE id='{$_SESSION['comment_id']}'";
	$results = $db->query($sql); 
	while($result = $results->fetch_assoc()){
		$name = $result['name'];
		$text = $result['text'];
	}
	
?>


<!DOCTYPE HTML>
<html>
    <head>
        <title>Sveiki, <?php echo $login_session; ?></title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="images/favicon.ico"/>
        <link rel="stylesheet" href="assets/css/main.css" />
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:'textarea' });</script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
			<?php if($login_type == 'admin'){
				include ('adminheader.php');
			} else {
				include('userheader.php');
			}
			?>
            <!-- Four -->
            <section id="four" class="wrapper special">
                <div class="container">
					<p>Cia galite modifikuoti komentarus</p>
					<form method="post" action="">
						<div class="form-group">
                             <label for="usr">Komentatoriaus vardas:</label>
							 <input name="name" type="text" class="form-control" id="usr" value='<?php echo $name?>' >
						</div>
						<br>
						<textarea name="content"><?php echo $text ?></textarea>
						<br>
						<input type="submit" value="Update" class="special" />
					</form>
  
                    <h3><?php if($update_success) {?> Komentaras atnaujintas! <?php } ?></h2>
					
				</div>
			</section>
		
            <!-- Footer -->
            <?php include ('footer.php'); ?>

        </div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>