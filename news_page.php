<?php
	include ('config.php');
	error_reporting(0);
	session_start();
	$user_check=$_SESSION['username'];
	$ses_sql = "SELECT username, type FROM users WHERE username='$user_check'";
	$result = $db->query($ses_sql); 
	$row = $result->fetch_assoc();
	$login_session =$row['username'];
	$login_type = $row['type'];
	$dberror = null;
	$comment_error = null;
	$currentuser = $login_session;
	
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		$_SESSION['postid'] = $_GET['postid'];
	}
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		switch($_POST['action']) {
			case "write_comment":
				if (isset($_SESSION['last_submit']) && time()-$_SESSION['last_submit'] < 60 && !isset($_SESSION['username'])){
					$comment_error = "Please wait for 60 seconds before another post";
				}
				else {
					$_SESSION['last_submit'] = time();
					$text = $_POST['content'];
					$text = stripslashes($text);
					$text = mysqli_real_escape_string($db, $text);
					$user = $_POST['name'];
					$user = stripslashes($user);
					$user = mysqli_real_escape_string($db, $user);
					$post_id = $_POST['post_id'];
					$sql = "INSERT INTO comments (text, post_id, name) VALUES ('" . $text . "', '" . $post_id . "', '" . $user . "')";
					if ($db->query($sql) === FALSE) {
						$error = "Db error";
					} else {
						echo "success!!";
					}
					break;
				}
				
			case "delete_comment":
				$comment_id=$_POST['var'];
					$sql = "DELETE FROM comments WHERE id='{$comment_id}'";
					$db->query($sql);
				break;
			case "approve_comment":
				$comment_id=$_POST['var'];
				$sql = "UPDATE comments SET approved= 'Yes' WHERE id='{$comment_id}'";
				$db->query($sql);
		}
	}
	
	$sql = "SELECT article_name, text, date, image,id, comment_status FROM user_content WHERE id='{$_SESSION['postid']}'";
	$sql2 = "SELECT text, time, name,id FROM comments WHERE post_id='{$_SESSION['postid']}' ORDER BY time DESC";
	$sql3 = "SELECT text, time, name,id FROM comments WHERE post_id='{$_SESSION['postid']}' AND approved='Yes' ORDER BY time DESC";
	$results = $db->query($sql); 
	$approved_results = $db->query($sql3);
	while($result = $results->fetch_assoc()){
		$article_name = $result['article_name'];
		$text = $result['text'];
		$date = $result['date'];
		$image = $result['image'];
		$id = $result['id'];
		$comment_status = $result['comment_status'];
	}
	$results2 = $db->query($sql2); 
	
?>


<!DOCTYPE HTML>
<html>
    <head>
        <title><?php echo $article_name; ?></title>
        <meta charset="utf-8" />
		<link rel="shortcut icon" href="images/favicon.ico"/>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:'textarea' });</script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
			<?php if($login_type == 'admin'){
				include ('adminheader.php');
			} else if($login_type == 'user'){
				include('userheader.php');
			} else {
				include('default_header.php');
			}
			?>
            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
					<div id="news" align="center" class="div_news">
						<h4><?php echo $article_name ?></h4>
						<p><?php echo strip_tags($text); ?> </p>
						<img src="images/<?php echo strip_tags($image);?> " height="150" width="150"><br>
						<span>Atnaujinta: <?php echo strip_tags($date); ?></span><br>
					</div>
					<br>
                
				<h3>Komentarai</h3>
				<?php
				if($comment_status == "approved-only" && $login_type != "admin") {
					while($result = $approved_results->fetch_assoc()){
						$comment_id = $result['id'];
					?>
					<div id="comments">
						<div id="comments-content">
							<h6><?php echo $result['name']; ?></h6>
							<p><?php echo $result['text']; ?></p>
							<span><?php echo $result['time'] ?></span>
							<?php if($login_type == 'admin'){ ?>
							<br>
									<a href="http://localhost/0907grupe3/update_comments.php?comment_id=<?php echo $comment_id?>"><button type="button" class="btn btn-default">Atnaujinti komentara</button></a>
									<form method="post" action='' class="deletebtn">
										<input type="submit" value="Istrinti komentara" class="special" />
										<input type="hidden" name="action" value="delete_comment">
										<input type="hidden" name="var" value='<?php echo "$comment_id";?>'>
									</form>		
							<?php
							}
							?>
							
						</div>
					</div>
				
				<?php
				} } else {
					while($result = $results2->fetch_assoc()){
						$comment_id = $result['id'];
				?>
				<div id="comments">
					<div id="comments-content">
						<h6><?php echo $result['name']; ?></h6>
						<p><?php echo $result['text']; ?></p>
						<span><?php echo $result['time'] ?></span>
						<?php if($login_type == 'admin'){ ?>
						<br>
								<a href="http://localhost/0907grupe3/update_comments.php?comment_id=<?php echo $comment_id?>"><button type="button" class="btn btn-default">Atnaujinti komentara</button></a>
								<form method="post" action='' class="deletebtn">
									<input type="submit" value="Istrinti komentara" class="special" />
									<input type="hidden" name="action" value="delete_comment">
									<input type="hidden" name="var" value='<?php echo "$comment_id";?>'>
								</form>		
								<?php
								if($comment_status == "approved-only" && $login_type == "admin") {
								?>
								<form method="post" action=''>
									<input type="submit" value="Patvirtinti komentara" class="special" />
									<input type="hidden" name="action" value="approve_comment">
									<input type="hidden" name="var" value='<?php echo "$comment_id";?>'>
								</form>
								<?php
								}	
								?>
						<?php
						}
						?>
						
					</div>
				</div>
				<?php
				}
				}
				?>
				<br>
				<?php
				if($comment_status == "allowed" || ($comment_status == "disallowed" && $login_type == "admin")) {
				?>
					<h3>Komentuoti</h3>
					<?php
						if($comment_error != null){
					?>
						<p>Komentuoti galima tik kas 1 min</p>
					<?php
						}
					?>
					<div id="write_comments">
					<form method="post" action="">
							<div class="form-group">
								<input name="name" type="text" class="form-control" id="usr" placeholder="Vardas" >
								<input type="hidden" name="post_id" value='<?php echo $id ?>'>
								<input type="hidden" name="action" value="write_comment">
								<br>
								<textarea name="content"></textarea>
								<br>
								<input type="submit" value="Rasyti" class="special" />
							</div>

					</form>
					</div>
				<?php
				}
				?>
				
				
            </section>

            
		</div>
            <!-- Footer -->
            <?php include ('footer.php'); ?>

        </div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>