<?php
	include ('session.php');
	$dberror = null;
	$currentuser = $login_session;
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$username = $_POST['username'];
		header('Location: admin_update_delete_news.php?user=' . $username);
	}
	$sql = "SELECT username FROM users";
	$users = $db->query($sql); 
?>


<!DOCTYPE HTML>
<html>
    <head>
        <title>Sveiki, <?php echo $login_session; ?></title>
        <meta charset="utf-8" />
		<link rel="shortcut icon" href="images/favicon.ico"/>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:'textarea' });</script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
			<?php include ('adminheader.php'); ?>
            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
					<h3>Pasirinkite, kurio vartotojo naujienas norėtumėte ištrinti/atnaujinti</h3>
					<form method="post" action="">
						<select name="username" multiple> 
						<?php while($user = $users->fetch_assoc()){
							$username = $user['username'];
						?>
						<option value='<?php echo "$username";?>'><?php echo $username; ?></option>
						<?php
						}
						?>
						</select>
						<br>
						<input type="submit" value="Select" class="special">
					</form>
					<br>
                </div>
            </section>

            <!-- Footer -->
            <?php include ('footer.php'); ?>

        </div>

        <!-- Scripts -->
		
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>