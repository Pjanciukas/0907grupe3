<?php
	include ('session.php');

	$dberror = null;
	$update_success = null;
	$text = null;
	$article_name = null;
	$file = null;
	$currentuser = $login_session;
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		$_SESSION['postid'] = $_GET['postid'];
		
	}
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$comment_status = "allowed";
		if($login_type == 'admin'){
			$comment_status = $_POST['comment-status'];
		}
		$text = $_POST['content'];
		$text = stripslashes($text);
		$text = mysqli_real_escape_string($db, $text);
		$article_name = $_POST['article_name'];
		$article_name = stripslashes($article_name);
		$article_name = mysqli_real_escape_string($db, $article_name);
		if(empty($_FILES['file']['name'])) {
			$sql = "UPDATE user_content SET article_name = '{$article_name}', text = '{$text}', comment_status = '{$comment_status}' WHERE id={$_SESSION['postid']}";
			if($db->query($sql) === FALSE) {
			$dberror = "Database error";
			} else {
				$update_success = "Update complete";
			}
		} else {
			$file = rand(1000,100000)."-".$_FILES['file']['name'];
			$file_loc = $_FILES['file']['tmp_name'];
			$folder="images/";
			$final_file=str_replace(' ','-',strtolower($file));
			$sql = "UPDATE user_content SET article_name = '{$article_name}', text = '{$text}', image = '{$final_file}', comment_status = '{$comment_status}' WHERE id={$_SESSION['postid']}";
			if($db->query($sql) === FALSE) {
			$dberror = "Database error";
			} else {
				$update_success = "Update complete";
			}
		}
	}
	
	$sql = "SELECT article_name, text, date, image FROM user_content WHERE id='{$_SESSION['postid']}'";
	$results = $db->query($sql); 
	while($result = $results->fetch_assoc()){
		$prev_article_name = $result['article_name'];
		$text = $result['text'];
	}
	
?>


<!DOCTYPE HTML>
<html>
    <head>
        <title>Sveiki, <?php echo $login_session; ?></title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="images/favicon.ico"/>
        <link rel="stylesheet" href="assets/css/main.css" />
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:'textarea' });</script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
			<?php if($login_type == 'admin'){
				include ('adminheader.php');
			} else {
				include('userheader.php');
			}
			?>
            <!-- Four -->
            <section id="four" class="wrapper special">
                <div class="container">
					<p>Čia galite atnaujinti savo naujienas</p>
					<form method="post" action="">
						<div class="form-group">
                             <label for="usr">Straipsnio pavadinimas:</label>
							 <input name="article_name" type="text" class="form-control" id="usr" value='<?php echo $prev_article_name?>' >
						</div>
						<br>
						<textarea name="content"><?php echo $text ?></textarea>
						<br>
						<input type="file" name="file" />
						<br>
						<?php if($login_type == 'admin'){ ?>
							<select name="comment-status" id="comment-status-style">
							  <option value="allowed">Leisti komentuoti</option>
							  <option value="disallowed">Neleisti komentuoti</option>
							  <option value="approved-only">Rodyti tik administratoriaus patvirtintus komentarus</option>
							</select>
						<?php } ?>
						<input type="submit" value="Atnaujinti" class="special" />
					</form>
  
                    <h3><?php if($update_success) {?> Naujienos atnaujintos! <?php } ?></h2>
					
				</div>
			</section>
		
            <!-- Footer -->
            <?php include ('footer.php'); ?>

        </div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>